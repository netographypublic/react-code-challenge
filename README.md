# Netography React Coding Challenge

Congratulations on making it to the coding challenge portion of the interview process!  This coding challenge was written by our front-end engineering team, and exercises most of the React technology and principles which we use in our codebase.

We recommend you to block ~2 hours of your time to complete it.

## Challenge:

Create a React application to visualize (graph) the number of SpaceX launches.  The application must allow HTML-form based filtering by spaceship names and the result of launches.  The end result should be something similar to the following graph.  We don’t expect pixel-perfect, in fact we encourage you to add a bit of your own “flair” to the final result.

![Example Result](netograph-code-challenge.gif)

## Conditions:

Please use the dataset of SpaceX launches (https://api.spacexdata.com/v3/launches/past) as your REST-based JSON API.

1. You must do/use the following:
1. Fetch the data asynchronously, but only 1 time 
1. Use React Functional Components
1. Use React Hooks
1. Use an application state manager  e.g. Redux, Redux-Toolkit, Redux-Saga or React.Context
1. Use an off-the-shelf javascript visualization library.  e.g. [highcharts](highcharts.com), [amcharts](amcharts.com) , [d3js](https://d3js.org/)
1. The data filters should be HTML form elements.  
   1. We know that most off-the-shelf graphing libraries will provide some filtering, but the filtering for ship name and launch result should be driven from outside of the graphing libraries.

### Extra Credit:

1. No in-line CSS.  Use SCSS or styled-components instead

## Acceptance:

1. Code & application are visible online.  e.g. in [codesandbox](codesandbox.io) or [codepen](codepen.io), etc. 
   1. You can use this template as a starting point: [React - CodeSandbox](https://codesandbox.io/s/react-new) 
1. Data is loaded dynamically, but only 1 time, from SpaceX
1. Interactive Charts allowing the ability to filter by ship name and launch outcome. 
